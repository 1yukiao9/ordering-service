import { Body, Controller, Get, Post } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dtos/create-order.dto';

@Controller('orders')
export class OrdersController {
    constructor(private readonly ordersService: OrdersService) {}

    @Post()
    async createOrder(@Body() body: CreateOrderDto) {
        return this.ordersService.createOrder(body);
    }

    @Get()
    getAllOrders() {
        return this.ordersService.getAllOrders();
    }
}
