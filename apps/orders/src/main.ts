import { NestFactory } from '@nestjs/core';
import { OrdersModule } from './orders.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
    const app = await NestFactory.create(OrdersModule);
    app.useGlobalPipes(new ValidationPipe());
    app.setGlobalPrefix('api');

    const configService = app.get(ConfigService);
    await app.listen(configService.get('PORT'));
}
bootstrap();
