import { Inject, Injectable } from '@nestjs/common';
import { OrdersRepository } from './orders.repository';
import { CreateOrderDto } from './dtos/create-order.dto';
import { BILLING_SERVICE } from './constants/services';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class OrdersService {
    constructor(
        private readonly ordersRepository: OrdersRepository,
        @Inject(BILLING_SERVICE) private readonly billingClient: ClientProxy,
    ) {}

    async createOrder(createOrderDto: CreateOrderDto) {
        const session = await this.ordersRepository.startTransaction();

        try {
            const order = await this.ordersRepository.create(createOrderDto, {
                session,
            });
            await lastValueFrom(
                this.billingClient.emit('order_created', {
                    createOrderDto,
                }),
            );
            await session.commitTransaction();
            return order;
        } catch (error) {
            console.log(error);
            await session.abortTransaction();
            throw error;
        }
    }

    getAllOrders() {
        return this.ordersRepository.find({});
    }
}
